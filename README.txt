Thanks go to the Meredith Corporation which originally sponsored this module.

This module was edited and taken over by Erica Scott on July 28th, 2016.
Added some extra implementation. This included:
- Adding some extra fields to the form so we can optionally include or exclude:
  - domain
  - channels
  - and tags from the script.
- Adding some validation to ensure the PID is exactly 24 characters.
- Setting the channels variable to the content type.
- Only adding tags to content that has a taxonomy field.
